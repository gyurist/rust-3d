# Rust 3D

## Prerequisites

-   Install the dependencies using `brew bundle`
-   Install the recommended Visual Studio Code extensions
-   Install [`cargo-edit`](https://github.com/killercup/cargo-edit) to be able to easily add new dependencies with `cargo add`

## Features

-   [x] `*.obj` file support
-   [x] Perspective projection
-   [x] Camera transformation (rotate camera around object)
-   [ ] Bariocentric coordinate based triangle rendering: https://github.com/ssloy/tinyrenderer/wiki/Lesson-2-Triangle-rasterization-and-back-face-culling
-   [x] Backface culling
-   [ ] Z-Buffer
-   [x] Flat shading
-   [ ] Gouraud shading
-   [ ] Phong shading
-   [ ] Pixel shaders
-   [ ] Shadow mapping
-   [ ] Multithreaded renderding
-   [ ] Simple bitmap text rendering

## Development

### Build

-   Use `cargo build` to create a debug build
-   Use `cargo build --release` to create a release build
-   Use `cargo run` to start the debug version, note that it is _considerably_ slower than the release one
-   Use `cargo run --release` to start the optimized release version
-   Use the `./bin/run-watch` script to start to process in watch mode

### Test

-   To execute tests in watch mode use `cargo watch -x test` or the `./bin/test-watch` script
