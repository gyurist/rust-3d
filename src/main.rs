mod lib;

use lib::Sandbox;

fn main() {
    let mut sandbox = Sandbox::new();
    sandbox.run();
}
