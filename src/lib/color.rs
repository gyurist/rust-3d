const RED: usize = 0;
const GREEN: usize = 1;
const BLUE: usize = 2;

#[derive(Default, Debug, Copy, Clone, PartialEq)]
pub struct Color {
    pub value: u32,
}

impl Color {
    pub fn new(value: u32) -> Color {
        Color { value }
    }
    pub fn of_rgb(rgb: [f32; 3]) -> Color {
        Color {
            value: ((rgb[RED] as u32) << 16) | ((rgb[GREEN] as u32) << 8) | (rgb[BLUE]) as u32,
        }
    }

    pub fn of_z_index(z: f32) -> Color {
        let i = 127.0 * z + 127.0;
        Color::of_rgb([i, i, i])
    }

    pub fn red(&self) -> f32 {
        ((self.value & 0xFF0000) >> 16) as f32
    }

    pub fn green(&self) -> f32 {
        ((self.value & 0x00FF00) >> 8) as f32
    }

    pub fn blue(&self) -> f32 {
        (self.value & 0x0000FF) as f32
    }

    pub fn with_intensity(self, intensity: f32) -> Color {
        if intensity < 0.0 {
            return Default::default();
        } else {
            return Color::of_rgb([
                self.red() * intensity,
                self.green() * intensity,
                self.blue() * intensity,
            ]);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn red() {
        assert_eq!(Color::new(0xFF0000).red(), 255.0);
    }

    #[test]
    fn green() {
        assert_eq!(Color::new(0x00FF00).green(), 255.0);
    }

    #[test]
    fn blue() {
        assert_eq!(Color::new(0x0000FF).blue(), 255.0);
    }

    #[test]
    fn with_intensity() {
        assert_eq!(Color::new(0x0000FF).with_intensity(0.5), Color::new(0x00007F));
    }
}
