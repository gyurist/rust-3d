use crate::lib::color::Color;
use crate::lib::image::{Image, Size};

pub fn generate_checker_pattern(size: Size) -> Image {
    let mut image = Image::new(size, size);
    let white = Color::new(0xFFFFFF);
    let black = Color::new(0x000000);
    for x in 0..image.width {
        for y in 0..image.height {
            let even_row = if (y / 8) % 2 == 0 { 1 } else { 0 };
            let even_column = if (x / 8) % 2 == 0 { 1 } else { 0 };
            let color = if (even_row + even_column) % 2 == 0 {
                white
            } else {
                black
            };
            image.set_pixel_with_depth(x, y, 0.0, color);
        }
    }
    return image;
}

#[derive(Default, Debug, Copy, Clone, PartialEq)]
pub struct TextureMapping {
    pub position: [f32; 2],
}

// FIXME: the convention is to use UV mapping instead of x/y
impl TextureMapping {
    pub fn x(&self) -> f32 {
        self.position[0]
    }

    pub fn y(&self) -> f32 {
        self.position[1]
    }
}
