use minifb::Key;
use serde::{Deserialize, Serialize};

use crate::lib::vector::Vector3D;

#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct Camera {
    pub position: Vector3D,
    pub rotation: Vector3D,
    pub zoom: f32,
}

impl Camera {
    pub fn r#move(&mut self, keys: Vec<Key>) {
        let distance = 0.01;
        let zoom = 1.1;
        for key in keys {
            match key {
                Key::W => self.position.value[0] += distance,
                Key::S => self.position.value[0] -= distance,
                Key::A => self.position.value[2] -= distance,
                Key::D => self.position.value[2] += distance,
                Key::Q => self.position.value[1] -= distance,
                Key::E => self.position.value[1] += distance,
                Key::R => self.zoom *= zoom,
                Key::F => self.zoom /= zoom,
                _ => (),
            }
        }
    }
    // FIXME: handle rotation properly
    pub fn rotate(&mut self, start: Vector3D, from: Option<(f32, f32)>, to: Option<(f32, f32)>) {
        if from.is_some() && to.is_some() {
            self.rotation.value[0] = start.value[0] + ((to.unwrap().1 - from.unwrap().1) * 4.0);
            self.rotation.value[1] = start.value[1] - ((to.unwrap().0 - from.unwrap().0) * 4.0);
        }
    }
}

impl Default for Camera {
    fn default() -> Self {
        Self {
            position: Default::default(),
            rotation: Default::default(),
            zoom: 1.0,
        }
    }
}
