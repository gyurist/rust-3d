use minifb::Key;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::prelude::*;

use crate::lib::camera::Camera;

#[derive(Default, Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct Config {
    pub draw_wireframe: bool,
    pub draw_normals: bool,
    pub draw_fill: bool,
    pub draw_bounding_box: bool,
    pub draw_axes: bool,
    pub draw_camera: bool,
    pub draw_cursor: bool,
    pub draw_grid: bool,
    pub draw_z_buffer: bool,
    pub camera: Camera,
}

const FILE_NAME: &str = "config.yaml";

impl Config {
    pub fn update(&mut self, keys: Vec<Key>) {
        for key in keys {
            match key {
                Key::Key1 => self.draw_wireframe = !self.draw_wireframe,
                Key::Key2 => self.draw_normals = !self.draw_normals,
                Key::Key3 => self.draw_fill = !self.draw_fill,
                Key::Key4 => self.draw_bounding_box = !self.draw_bounding_box,
                Key::Key5 => self.draw_axes = !self.draw_axes,
                Key::Key6 => self.draw_camera = !self.draw_camera,
                Key::Key7 => self.draw_cursor = !self.draw_cursor,
                Key::Key8 => self.draw_grid = !self.draw_grid,
                Key::Key9 => self.draw_z_buffer = !self.draw_z_buffer,
                _ => (),
            }
        }
    }

    pub fn save(&self) -> std::io::Result<()> {
        let config_str = serde_yaml::to_string(self).unwrap();
        let mut file = File::create(FILE_NAME)?;
        file.write_all(config_str.as_bytes())?;
        Ok(())
    }

    pub fn load() -> std::io::Result<Config> {
        let default_config: Config = Default::default();
        match Config::read_config_string() {
            Ok(config_str) => {
                let loaded_config: Result<Config, serde_yaml::Error> = serde_yaml::from_str(config_str.as_str());
                return match loaded_config {
                    Ok(config) => Ok(config),
                    Err(_) => Ok(default_config),
                };
            }
            Err(_) => Ok(default_config),
        }
    }

    fn read_config_string() -> std::io::Result<String> {
        let mut file = File::open(FILE_NAME)?;
        let mut config_str = String::new();
        file.read_to_string(&mut config_str)?;
        return Ok(config_str);
    }
}

// TODO: add tests for config loading
