use crate::lib::color::Color;
use crate::lib::texture::TextureMapping;
use crate::lib::vector::Vector3D;

const X: usize = 0;
const Y: usize = 1;
const Z: usize = 2;
const RED: usize = 3;
const GREEN: usize = 4;
const BLUE: usize = 5;
const TEXTURE_X: usize = 6;
const TEXTURE_Y: usize = 7;

#[derive(Default, Debug, Copy, Clone, PartialEq)]
pub struct Vertex {
    pub position: Vector3D,
    pub color: Color,
    pub texture: TextureMapping,
}

impl Vertex {
    pub fn set_position(&mut self, vector: Vector3D) {
        self.position = vector;
    }

    pub fn interpolate(self, to: Vertex, steps: f32) -> interpolation::LinearInterpolation {
        interpolation::LinearInterpolation::new(self.into(), to.into(), steps)
    }
}

interpolation!(interpolation, [f32; 8]);

impl Into<interpolation::Value> for Vertex {
    fn into(self) -> interpolation::Value {
        [
            self.position.x(),
            self.position.y(),
            self.position.z(),
            self.color.red(),
            self.color.green(),
            self.color.blue(),
            self.texture.x(),
            self.texture.y(),
        ]
    }
}

impl From<&interpolation::Value> for Vertex {
    fn from(v: &interpolation::Value) -> Self {
        Vertex {
            position: Vector3D::new([v[X], v[Y], v[Z]]),
            color: Color::of_rgb([v[RED], v[GREEN], v[BLUE]]),
            texture: TextureMapping {
                position: [v[TEXTURE_X], v[TEXTURE_Y]],
            },
        }
    }
}

impl From<interpolation::Value> for Vertex {
    fn from(v: interpolation::Value) -> Self {
        Vertex {
            position: Vector3D::new([v[X], v[Y], v[Z]]),
            color: Color::of_rgb([v[RED], v[GREEN], v[BLUE]]),
            texture: TextureMapping {
                position: [v[TEXTURE_X], v[TEXTURE_Y]],
            },
        }
    }
}

impl From<&Vertex> for interpolation::Value {
    fn from(v: &Vertex) -> Self {
        v.into()
    }
}
