use crate::lib::image::Image;
use crate::lib::vector::Vector3D;

#[derive(Default, Debug, Copy, Clone, PartialEq)]
pub struct BoundingBox {
    pub min: Vector3D,
    pub max: Vector3D,
    pub width: f32,
    pub height: f32,
    pub depth: f32,
}

// TODO: should have pixel based implementation as well
// TODO; create a trait for object, face, image to return bounding box
impl BoundingBox {
    pub fn of_vectors(vectors: &[Vector3D]) -> BoundingBox {
        let mut min = vectors[0];
        let mut max = min;
        for v in vectors {
            for i in 0..3 {
                if v.value[i] < min.value[i] {
                    min.value[i] = v.value[i];
                } else if v.value[i] > max.value[i] {
                    max.value[i] = v.value[i];
                }
            }
        }
        BoundingBox {
            min,
            max,
            width: (max.x() - min.x()).abs(),
            height: (max.y() - min.y()).abs(),
            depth: (max.z() - min.z()).abs(),
        }
    }

    // FIXME: take the depth of camera into consideration
    pub fn of_image(image: &Image) -> BoundingBox {
        BoundingBox::of_vectors(&[
            Vector3D::new([0.0, 0.0, f32::NEG_INFINITY]),
            Vector3D::new([image.width as f32, image.height as f32, f32::INFINITY]),
        ])
    }

    pub fn intersects(&self, b: &BoundingBox) -> bool {
        let mut result = true;
        for i in 0..3 {
            result = result && !(self.max.value[i] < b.min.value[i] || self.min.value[i] > b.max.value[i])
        }
        return result;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new() {
        assert_eq!(
            BoundingBox::of_vectors(&[
                Vector3D::new([1028.9219, 324.8868, -82.97654]),
                Vector3D::new([211.57954, 482.74164, 91.403595]),
                Vector3D::new([781.013, 728.84186, 363.26718])
            ]),
            BoundingBox::of_vectors(&[
                Vector3D::new([211.57954, 324.8868, -82.97654]),
                Vector3D::new([1028.9219, 728.84186, 363.26718])
            ]),
        );
    }

    #[test]
    fn intersect() {
        test_intersect(
            [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
            [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0]],
            true,
        );
        test_intersect(
            [[0.0, 0.0, 0.0], [1.0, 0.0, 0.0]],
            [[0.0, 0.0, 0.0], [1.0, 0.0, 0.0]],
            true,
        );
        test_intersect(
            [[0.0, 0.0, 0.0], [2.0, 0.0, 0.0]],
            [[1.0, 0.0, 0.0], [3.0, 0.0, 0.0]],
            true,
        );
        test_intersect(
            [[1.0, 0.0, 0.0], [3.0, 0.0, 0.0]],
            [[0.0, 0.0, 0.0], [2.0, 0.0, 0.0]],
            true,
        );
        test_intersect(
            [[0.0, 0.0, 0.0], [3.0, 0.0, 0.0]],
            [[1.0, 0.0, 0.0], [2.0, 0.0, 0.0]],
            true,
        );
        test_intersect(
            [[1.0, 0.0, 0.0], [2.0, 0.0, 0.0]],
            [[0.0, 0.0, 0.0], [3.0, 0.0, 0.0]],
            true,
        );
        test_intersect(
            [[0.0, 0.0, 0.0], [1.0, 0.0, 0.0]],
            [[2.0, 0.0, 0.0], [3.0, 0.0, 0.0]],
            false,
        );
        test_intersect(
            [[2.0, 0.0, 0.0], [3.0, 0.0, 0.0]],
            [[0.0, 0.0, 0.0], [1.0, 0.0, 0.0]],
            false,
        );
        test_intersect(
            [[2.0, 0.0, 0.0], [3.0, 0.0, 0.0]],
            [[0.0, 0.0, 0.0], [1.0, 0.0, 0.0]],
            false,
        );
        test_intersect(
            [[0.0, 0.0, 0.0], [1.0, 1.0, 0.0]],
            [[0.0, 2.0, 0.0], [1.0, 3.0, 0.0]],
            false,
        );
        test_intersect(
            [[0.0, 2.0, 0.0], [1.0, 3.0, 0.0]],
            [[0.0, 0.0, 0.0], [1.0, 1.0, 0.0]],
            false,
        );
        test_intersect(
            [[0.0, 1.0, 0.0], [3.0, 2.0, 0.0]],
            [[1.0, 0.0, 0.0], [2.0, 3.0, 0.0]],
            true,
        );
    }

    fn test_intersect(a: [[f32; 3]; 2], b: [[f32; 3]; 2], expected: bool) {
        assert_eq!(
            BoundingBox::of_vectors(&[Vector3D::new(a[0]), Vector3D::new(a[1])])
                .intersects(&BoundingBox::of_vectors(&[Vector3D::new(b[0]), Vector3D::new(b[1])])),
            expected
        );
    }
}
