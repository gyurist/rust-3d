use crate::lib::image::Image;
use crate::lib::matrix::{Matrix, IDENTITY};
use crate::lib::vector::Vector3D;

#[derive(Default, Debug, Copy, Clone, PartialEq)]
pub struct Transformation {
    value: Matrix,
}

impl Transformation {
    pub fn new() -> Transformation {
        Transformation { value: IDENTITY }
    }

    pub fn scale(mut self, factor: Vector3D) -> Transformation {
        self.value = Matrix::new([
            [factor.x(), 0.0, 0.0, 0.0],
            [0.0, factor.y(), 0.0, 0.0],
            [0.0, 0.0, factor.z(), 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ]) * self.value;
        return self;
    }

    pub fn translate(mut self, vector: Vector3D) -> Transformation {
        self.value = Matrix::new([
            [1.0, 0.0, 0.0, vector.x()],
            [0.0, 1.0, 0.0, vector.y()],
            [0.0, 0.0, 1.0, vector.z()],
            [0.0, 0.0, 0.0, 1.0],
        ]) * self.value;
        return self;
    }

    pub fn project(mut self, distance: f32) -> Transformation {
        // TODO: check if -1.0 / distance defines the center of projection on the z/index
        //       this should allow to properly set the bounding box for the view
        self.value = Matrix::new([
            [1.0, 0.0, 0.0, 0.0],
            [0.0, 1.0, 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
            [0.0, 0.0, -1.0 / distance, 1.0],
        ]) * self.value;
        return self;
    }

    pub fn rotate(mut self, rotation: Vector3D) -> Transformation {
        let alpha = rotation.x();
        let beta = rotation.y();
        let gamma = rotation.z();
        self.value = Matrix::new([
            [
                alpha.cos() * beta.cos(),
                alpha.cos() * beta.sin() * gamma.sin() - alpha.sin() * gamma.cos(),
                alpha.cos() * beta.sin() * gamma.cos() + alpha.sin() * gamma.sin(),
                0.0,
            ],
            [
                alpha.sin() * beta.cos(),
                alpha.sin() * beta.sin() * gamma.sin() + alpha.cos() * gamma.cos(),
                alpha.sin() * beta.sin() * gamma.cos() - alpha.cos() * gamma.sin(),
                0.0,
            ],
            [-beta.sin(), beta.cos() * gamma.cos(), 1.0, 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ]) * self.value;
        return self;
    }

    pub fn look(mut self, from: Vector3D, to: Vector3D) -> Transformation {
        let up = Vector3D::new([0.0, 1.0, 0.0]);
        let z = (from - to).normalize();
        let x = (up * z).normalize();
        let y = (z * x).normalize();

        let minv = Matrix::new([
            [x.x(), x.y(), x.z(), 0.0],
            [y.x(), y.y(), y.z(), 0.0],
            [z.x(), z.y(), z.z(), 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ]);
        let translate = Matrix::new([
            [1.0, 0.0, 0.0, -to.x()],
            [0.0, 1.0, 0.0, -to.y()],
            [0.0, 0.0, 1.0, -to.z()],
            [0.0, 0.0, 0.0, 1.0],
        ]);
        self.value = (minv * translate) * self.value;
        return self;
    }

    pub fn perspective_look(self, from: Vector3D, to: Vector3D) -> Transformation {
        return self.look(from, to).project(from.distance(to));
    }

    pub fn zoom(self, factor: f32) -> Transformation {
        return self.scale(Vector3D::new([factor, factor, 255.0]));
    }

    pub fn view(self, image: &Image) -> Transformation {
        return self.scale(Vector3D::new([1.0, -1.0, 1.0])).translate(Vector3D::new([
            (image.width / 2) as f32,
            (image.height / 2) as f32,
            0.0,
        ]));
    }

    // TODO: create trait for vector, face, object, etc. to apply transforms
    pub fn apply(&self, vector: Vector3D) -> Vector3D {
        return self.value * vector;
    }
}
