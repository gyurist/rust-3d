use minifb;
use std::time;

use crate::lib::image::{Image, Size};

pub struct Window {
    window: minifb::Window,
}

impl Window {
    pub fn new(width: Size, height: Size, max_framerate: u32) -> Window {
        let mut window = minifb::Window::new(
            "ESC to exit",
            width as usize,
            height as usize,
            minifb::WindowOptions {
                resize: true,
                scale: minifb::Scale::X1,
                ..minifb::WindowOptions::default()
            },
        )
        .unwrap_or_else(|e| {
            panic!("{}", e);
        });
        let frame_duration = time::Duration::from_millis(1000 / max_framerate as u64);
        window.limit_update_rate(Some(frame_duration));
        Window { window: window }
    }

    pub fn is_open(&self) -> bool {
        self.window.is_open() && !self.window.is_key_down(minifb::Key::Escape)
    }

    pub fn update(&mut self, image: &Image) {
        self.window
            .update_with_buffer(&image.buffer, image.width as usize, image.height as usize)
            .unwrap();
    }

    pub fn is_left_mouse_down(&self) -> bool {
        self.window.get_mouse_down(minifb::MouseButton::Left)
    }

    pub fn get_mouse_pos(&self) -> Option<(f32, f32)> {
        self.window.get_mouse_pos(minifb::MouseMode::Discard)
    }

    // TODO: add a reference point
    pub fn get_mouse_pos_rel(&self) -> Option<(f32, f32)> {
        let window_size = self.window.get_size();
        self.get_mouse_pos().map(|pos| {
            let x = pos.0 / window_size.0 as f32;
            let y = pos.1 / window_size.1 as f32;
            (x, y)
        })
    }

    pub fn get_keys_down(&self) -> Vec<minifb::Key> {
        self.window.get_keys()
    }

    pub fn get_keys_pressed(&self) -> Vec<minifb::Key> {
        self.window.get_keys_pressed(minifb::KeyRepeat::No)
    }
}
