use std::ops;

use crate::lib::vector::Vector3D;

pub const IDENTITY: Matrix = Matrix {
    value: [
        [1.0, 0.0, 0.0, 0.0],
        [0.0, 1.0, 0.0, 0.0],
        [0.0, 0.0, 1.0, 0.0],
        [0.0, 0.0, 0.0, 1.0],
    ],
};

type Value = [[f32; 4]; 4];

// TODO: generalize once https://github.com/rust-lang/rfcs/blob/master/text/2000-const-generics.md is available
#[derive(Default, Debug, Copy, Clone, PartialEq)]
pub struct Matrix {
    value: Value,
}

impl Matrix {
    pub fn new(value: Value) -> Matrix {
        Matrix { value: value }
    }
}

impl ops::Mul<Matrix> for Matrix {
    type Output = Matrix;

    fn mul(self, rhs: Matrix) -> Matrix {
        let mut result: Matrix = Default::default();
        for i in 0..self.value.len() {
            for j in 0..self.value[i].len() {
                for k in 0..rhs.value.len() {
                    result.value[i][j] = result.value[i][j] + self.value[i][k] * rhs.value[k][j];
                }
            }
        }
        return result;
    }
}

impl ops::Mul<Vector3D> for Matrix {
    type Output = Vector3D;

    fn mul(self, rhs: Vector3D) -> Vector3D {
        let mut result = [0.0; 4];
        let rhs_h = [rhs.x(), rhs.y(), rhs.z(), 1.0];
        for i in 0..self.value.len() {
            for j in 0..self.value[i].len() {
                result[i] = self.value[i][j] * rhs_h[j] + result[i];
            }
        }
        let w = result[3];
        return Vector3D::new([result[0] / w, result[1] / w, result[2] / w]);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn multiply_identity() {
        assert_eq!(IDENTITY * IDENTITY, IDENTITY);
    }

    #[test]
    fn multiply_with_identity() {
        let a = Matrix::new([
            [1.0, 0.0, 0.0, 0.0],
            [0.0, 2.0, 0.0, 0.0],
            [0.0, 0.0, 3.0, 0.0],
            [0.0, 0.0, 0.0, 4.0],
        ]);
        assert_eq!(a * IDENTITY, a);
    }

    #[test]
    fn associativity() {
        let a = Matrix::new([
            [1.0, 0.0, 0.0, 1.0],
            [0.0, 2.0, 0.0, 2.0],
            [0.0, 0.0, 3.0, 3.0],
            [0.0, 0.0, 0.0, 4.0],
        ]);
        let b = Matrix::new([
            [5.0, 0.0, 0.0, 0.0],
            [0.0, 6.0, 0.0, 0.0],
            [0.0, 0.0, 7.0, 0.0],
            [0.0, 0.0, 0.0, 8.0],
        ]);
        let c = Matrix::new([
            [9.0, 0.0, 0.0, 0.0],
            [0.0, 10.0, 0.0, 0.0],
            [0.0, 0.0, 11.0, 0.0],
            [0.0, 0.0, -0.2, 12.0],
        ]);
        assert_eq!((a * b) * c, a * (b * c));
    }

    #[test]
    fn multiply_identity_with_vector3d() {
        let q = Vector3D::new([1.0, 2.0, 3.0]);
        assert_eq!(IDENTITY * q, q);
    }

    #[test]
    fn multiply_with_vector3d() {
        let q = Vector3D::new([1.0, 2.0, 3.0]);
        let a = Matrix::new([
            [1.0, 0.0, 0.0, 0.0],
            [0.0, 2.0, 0.0, 0.0],
            [0.0, 0.0, 3.0, 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ]);
        let aq = Vector3D::new([1.0, 4.0, 9.0]);
        assert_eq!(a * q, aq);
    }

    #[test]
    fn associativity_with_vector3d() {
        let a = Matrix::new([
            [1.0, 0.0, 0.0, 1.0],
            [0.0, 2.0, 0.0, 2.0],
            [0.0, 0.0, 3.0, 3.0],
            [0.0, 0.0, 0.0, 4.0],
        ]);
        let b = Matrix::new([
            [5.0, 0.0, 0.0, 0.0],
            [0.0, 6.0, 0.0, 0.0],
            [0.0, 0.0, 7.0, 0.0],
            [0.0, 0.0, 0.0, 8.0],
        ]);
        let c = Matrix::new([
            [9.0, 0.0, 0.0, 0.0],
            [0.0, 10.0, 0.0, 0.0],
            [0.0, 0.0, 11.0, 0.0],
            [0.0, 0.0, -0.2, 12.0],
        ]);
        let q = Vector3D::new([1.0, 2.0, 3.0]);
        assert_eq!(a * b * c * q, (a * b * c) * q);
    }
}
