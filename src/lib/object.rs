use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

use crate::lib::color::Color;
use crate::lib::image::Image;
use crate::lib::vector::Vector3D;
use crate::lib::vertex::Vertex;

#[derive(Default, Debug)]
pub struct Face<'a> {
    pub vertices: [Vertex; 3],
    pub outline: Option<Color>,
    // TODO: use texture index instead of pointer to simplify
    pub texture: Option<&'a Image>,
    pub normal: Vector3D,
    pub center: Vector3D,
}

impl Face<'_> {
    pub fn new(vertices: [Vertex; 3], outline: Option<Color>, texture: Option<&Image>) -> Face {
        let normal =
            ((vertices[1].position - vertices[0].position) * (vertices[2].position - vertices[0].position)).normalize();
        let center = (vertices[0].position + vertices[1].position + vertices[2].position) / 3.0;
        Face {
            vertices,
            outline,
            texture,
            normal,
            center,
        }
    }
}

#[derive(Debug)]
pub struct Object<'a> {
    pub faces: Vec<Face<'a>>,
}

pub fn load_dummy<'a>(textures: &'a [Image]) -> Object<'a> {
    let outline = Color::new(0xFFFFFF);
    let size = 0.5;
    let a: Vertex = Vertex::from([-size, 0.0, size, 255.0, 0.0, 0.0, 0.0, 0.0]);
    let b: Vertex = Vertex::from([-size, 0.0, -size, 0.0, 255.0, 0.0, 0.0, 255.0]);
    let c: Vertex = Vertex::from([size, 0.0, -size, 0.0, 0.0, 255.0, 255.0, 255.0]);
    let d: Vertex = Vertex::from([size, 0.0, size, 0.0, 255.0, 0.0, 0.0, 0.0]);
    Object {
        faces: vec![
            Face::new([c, b, a], Some(outline), Some(&textures[0])),
            Face::new([d, c, a], Some(outline), None),
        ],
    }
}

pub fn load(filename: &str) -> Object {
    let mut vertices: Vec<Vertex> = vec![];
    let mut faces: Vec<Face> = vec![];
    let outline = Color::new(0xAAAAAA);
    let fill = Color::new(0xFFFFFF);
    let lines = read_lines_as_str(filename);
    let mut min = [f32::MAX; 3];
    let mut max = [f32::MIN; 3];
    for line in lines.iter() {
        if line.as_str().starts_with("v ") {
            let mut v: Vertex = Default::default();
            let values: Vec<f32> = line
                .split_whitespace()
                .skip(1)
                .map(|n| n.parse::<f32>().unwrap_or_default())
                .collect();
            let mut i = 0;
            for value in values {
                v.position.value[i] = value;
                if value < min[i] {
                    min[i] = value;
                }
                if value > max[i] {
                    max[i] = value;
                }
                i = i + 1;
            }
            v.color = fill;
            vertices.push(v);
        }
    }
    // TODO: use bounding box implemenation here
    let mut range = f32::MIN;
    for i in 0..3 {
        if max[i] - min[i] > range {
            range = max[i] - min[i];
        }
    }
    for v in vertices.iter_mut() {
        v.position = v.position / range;
    }
    // TODO: load normal vectors from obj
    for line in lines.iter() {
        if line.as_str().starts_with("f ") {
            let vertex_indices: Vec<usize> = line
                .split_whitespace()
                .skip(1)
                .map(|n| {
                    n.split("/")
                        .next()
                        .unwrap_or_default()
                        .parse::<usize>()
                        .unwrap_or_default()
                })
                .collect();
            let mut index = 0;
            let mut face_vertices: [Vertex; 3] = Default::default();
            for vertex_index in vertex_indices.iter() {
                match vertices.get(vertex_index - 1).cloned() {
                    Some(v) => {
                        face_vertices[index] = v;
                    }
                    None => {}
                }
                index = index + 1;
            }
            faces.push(Face::new(face_vertices, Some(outline), None));
        }
    }
    println!("Loaded {:?}, faces: {:?}", filename, faces.len());
    return Object { faces: faces };
}

fn read_lines_as_str<P>(filename: P) -> Vec<String>
where
    P: AsRef<Path>,
{
    let mut result: Vec<String> = vec![];
    if let Ok(lines) = read_lines(filename) {
        for line in lines {
            if let Ok(ip) = line {
                result.push(ip);
            }
        }
    }
    return result;
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    return Ok(io::BufReader::new(file).lines());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn load() {
        // TODO: write test
    }
}
