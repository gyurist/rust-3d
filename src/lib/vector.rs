use serde::{Deserialize, Serialize};
use std::ops;

const X: usize = 0;
const Y: usize = 1;
const Z: usize = 2;

// TODO: generalize once https://github.com/rust-lang/rfcs/blob/master/text/2000-const-generics.md is available
type Type = f32;
pub type Value = [Type; 3];

#[derive(Default, Debug, Copy, Clone, PartialEq, Deserialize, Serialize)]
pub struct Vector3D {
    pub value: Value,
}

impl Vector3D {
    pub fn new(value: Value) -> Vector3D {
        Vector3D { value }
    }

    pub fn length(&self) -> Type {
        return (self.value[X].powi(2) + self.value[Y].powi(2) + self.value[Z].powi(2)).sqrt();
    }

    pub fn normalize(&self) -> Vector3D {
        let length = 1.0 / self.length();
        let mut value: Value = Default::default();
        for i in 0..self.value.len() {
            value[i] = self.value[i] * length
        }
        return Vector3D { value };
    }

    pub fn distance(self, v: Vector3D) -> Type {
        return (self - v).length();
    }

    pub fn dot(self, v: Vector3D) -> Type {
        let mut result = 0.0;
        for i in 0..self.value.len() {
            result += self.value[i] * v.value[i];
        }
        return result;
    }

    pub fn x(&self) -> Type {
        return self.value[X];
    }

    pub fn y(&self) -> Type {
        return self.value[Y];
    }

    pub fn z(&self) -> Type {
        return self.value[Z];
    }

    pub fn interpolate(self, to: Vector3D, steps: f32) -> interpolation::LinearInterpolation {
        return interpolation::LinearInterpolation::new(self.into(), to.into(), steps);
    }
}

interpolation!(interpolation, [f32; 3]);

impl ops::Mul<Vector3D> for Vector3D {
    type Output = Vector3D;

    fn mul(self, rhs: Vector3D) -> Vector3D {
        let value = [
            self.value[Y] * rhs.value[Z] - self.value[Z] * rhs.value[Y],
            self.value[Z] * rhs.value[X] - self.value[X] * rhs.value[Z],
            self.value[X] * rhs.value[Y] - self.value[Y] * rhs.value[X],
        ];
        return Vector3D { value };
    }
}

impl ops::Add<Vector3D> for Vector3D {
    type Output = Vector3D;

    fn add(self, rhs: Vector3D) -> Vector3D {
        let mut value: Value = Default::default();
        for i in 0..self.value.len() {
            value[i] = self.value[i] + rhs.value[i];
        }
        return Vector3D { value };
    }
}

impl ops::Sub<Vector3D> for Vector3D {
    type Output = Vector3D;

    fn sub(self, rhs: Vector3D) -> Vector3D {
        let mut value: Value = Default::default();
        for i in 0..self.value.len() {
            value[i] = self.value[i] - rhs.value[i];
        }
        return Vector3D { value };
    }
}

impl ops::Mul<f32> for Vector3D {
    type Output = Vector3D;

    fn mul(self, rhs: f32) -> Vector3D {
        let mut value: Value = Default::default();
        for i in 0..self.value.len() {
            value[i] = self.value[i] * rhs;
        }
        return Vector3D { value };
    }
}

impl ops::Div<f32> for Vector3D {
    type Output = Vector3D;

    fn div(self, rhs: f32) -> Vector3D {
        if rhs != 0.0 {
            let mut value: Value = Default::default();
            for i in 0..self.value.len() {
                value[i] = self.value[i] / rhs;
            }
            return Vector3D { value };
        } else {
            return self;
        }
    }
}

impl Into<interpolation::Value> for Vector3D {
    fn into(self) -> interpolation::Value {
        [self.value[X], self.value[Y], self.value[Z]]
    }
}

impl From<&interpolation::Value> for Vector3D {
    fn from(v: &interpolation::Value) -> Self {
        Vector3D {
            value: [v[0], v[1], v[2]],
        }
    }
}

impl From<&Vector3D> for interpolation::Value {
    fn from(v: &Vector3D) -> Self {
        v.into()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn length() {
        let v = Vector3D::new([1.0, 1.0, 1.0]);
        assert_eq!(v.length(), 1.7320508)
    }

    #[test]
    fn normalize() {
        let v = Vector3D::new([2.0, 2.0, 2.0]);
        assert_eq!(v.normalize(), Vector3D::new([0.57735026, 0.57735026, 0.57735026]));
    }

    #[test]
    fn distance() {
        let a = Vector3D::new([2.0, 0.0, 0.0]);
        let b = Vector3D::new([0.0, 0.0, 0.0]);
        assert_eq!(a.distance(b), 2.0);
    }

    #[test]
    fn addition() {
        let a = Vector3D::new([1.0, 2.0, 3.0]);
        let b = Vector3D::new([4.0, 5.0, 6.0]);
        assert_eq!(a + b, Vector3D::new([5.0, 7.0, 9.0]));
    }

    #[test]
    fn subtraction() {
        let a = Vector3D::new([4.0, 5.0, 6.0]);
        let b = Vector3D::new([1.0, 2.0, 3.0]);
        assert_eq!(a - b, Vector3D::new([3.0, 3.0, 3.0]));
    }

    #[test]
    fn cross_product() {
        let a = Vector3D::new([4.0, 5.0, 6.0]);
        let b = Vector3D::new([1.0, 2.0, 3.0]);
        assert_eq!(a * b, Vector3D::new([3.0, -6.0, 3.0]));
    }

    #[test]
    fn dot_product() {
        let a = Vector3D::new([4.0, 5.0, 6.0]);
        let b = Vector3D::new([1.0, 2.0, 3.0]);
        assert_eq!(a.dot(b), 32.0);
    }

    #[test]
    fn multiply() {
        let v = Vector3D::new([4.0, 5.0, 6.0]);
        assert_eq!(v * 2.0, Vector3D::new([8.0, 10.0, 12.0]));
    }

    #[test]
    fn divide() {
        let v = Vector3D::new([4.0, 5.0, 6.0]);
        assert_eq!(v / 2.0, Vector3D::new([2.0, 2.5, 3.0]));
    }
}
