#[macro_export]
macro_rules! interpolation {
    ($name: ident, $type: ty) => {
        pub mod $name {
            pub type Value = $type;

            #[derive(Default, Debug, Copy, Clone, PartialEq)]
            pub struct LinearInterpolation {
                pub value: $type,
                pub finished: bool,
                to: $type,
                step: $type,
                steps: f32,
            }

            impl LinearInterpolation {
                pub fn new(from: Value, to: Value, mut steps: f32) -> LinearInterpolation {
                    let mut step: Value = Default::default();
                    let mut value = from;
                    let mut finished = false;
                    steps = steps.abs();
                    if steps < 1.0 {
                        value = to;
                        finished = true;
                    } else {
                        for i in 0..from.len() {
                            step[i] = (to[i] - from[i]) / steps;
                        }
                    }
                    LinearInterpolation {
                        value,
                        finished,
                        step,
                        steps,
                        to,
                    }
                }
                pub fn advance(&mut self) {
                    if self.steps < 1.0 {
                        self.value = self.to;
                        self.finished = true;
                    } else {
                        for i in 0..self.value.len() {
                            self.value[i] = self.value[i] + self.step[i];
                        }
                        self.steps -= 1.0;
                    }
                }
                pub fn advance_by(&mut self, mut steps: f32) {
                    steps = steps.abs();
                    if self.steps < 1.0 {
                        self.value = self.to;
                        self.finished = true;
                    } else {
                        for i in 0..self.value.len() {
                            self.value[i] = self.value[i] + (self.step[i] * steps);
                        }
                        self.steps -= steps;
                    }
                }
            }
        }
    };
}
