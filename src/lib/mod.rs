extern crate log;
extern crate pretty_env_logger;

use log::info;
use std::time::{Duration, Instant};

mod bounding_box;
mod camera;
mod color;
mod config;
mod draw;
mod image;
#[macro_use]
mod interpolation;
mod matrix;
mod object;
mod texture;
mod transform;
mod vector;
mod vertex;
mod window;

use config::Config;
use draw::draw;
use image::Image;
use object::{load, load_dummy};
use texture::generate_checker_pattern;
use vector::Vector3D;
use window::Window;

pub struct Sandbox {
    config: Config,
}

impl Sandbox {
    pub fn new() -> Sandbox {
        Sandbox {
            config: Config::load().unwrap(),
        }
    }

    pub fn run(&mut self) {
        pretty_env_logger::init();
        info!("Init");
        let mut image = Image::new(1280, 800);
        let mut window = Window::new(image.width, image.height, 100);
        let mut frame: u32 = 0;

        let textures = [generate_checker_pattern(64)];
        let objects = [
            load_dummy(&textures),
            load("./assets/teapot.obj")
            // load("./assets/a380.obj"),
            // load("./assets/cube.obj"),
        ];

        // TODO: extract to stats
        let mut frame_duration: Duration = Default::default();
        let mut total_triangles = 0;
        let mut rotation_start: Option<Vector3D> = None;
        let mut mouse_pos_from: Option<(f32, f32)> = None;
        let sample_frames = 100;

        while window.is_open() {
            let cursor = window.get_mouse_pos();
            let keys_down = window.get_keys_down();
            self.config.camera.r#move(keys_down);
            if window.is_left_mouse_down() {
                let mouse_pos_to = window.get_mouse_pos_rel();
                if rotation_start.is_none() {
                    rotation_start = Some(self.config.camera.rotation);
                    mouse_pos_from = mouse_pos_to;
                }
                self.config
                    .camera
                    .rotate(rotation_start.unwrap(), mouse_pos_from, mouse_pos_to);
            } else {
                rotation_start = None;
            }
            let keys_pressed = window.get_keys_pressed();
            self.config.update(keys_pressed);
            let frame_start = Instant::now();
            let triangles = draw(&mut image, &objects, frame, cursor, self.config);
            frame_duration = frame_duration + frame_start.elapsed();
            total_triangles = total_triangles + triangles;
            if frame > 0 && frame % sample_frames == 0 {
                let avg_frame_duration = frame_duration.as_micros() as f32 / sample_frames as f32;
                println!(
                    "{:3.3} fps - avg frame duration {:4.3} ms - avg triangles: {:4.3}",
                    1000000.0 / avg_frame_duration,
                    avg_frame_duration / 1000.0,
                    total_triangles as f32 / sample_frames as f32
                );
                total_triangles = 0;
                frame_duration = Default::default();
            }
            window.update(&image);
            frame = frame + 1;
        }
    }
}

impl Drop for Sandbox {
    fn drop(&mut self) {
        self.config.save();
    }
}
