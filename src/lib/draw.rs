use crate::lib::bounding_box::BoundingBox;
use crate::lib::color::Color;
use crate::lib::config::Config;
use crate::lib::image::{Image, Size};
use crate::lib::object::{Face, Object};
use crate::lib::transform::Transformation;
use crate::lib::vector::interpolation::LinearInterpolation as VectorInterpolation;
use crate::lib::vector::Vector3D;
use crate::lib::vertex::interpolation::LinearInterpolation as VertexInterpolation;
use crate::lib::vertex::Vertex;

pub fn draw(image: &mut Image, objects: &[Object], _frame: u32, cursor: Option<(f32, f32)>, config: Config) -> u32 {
    image.clear();
    let camera = config.camera;
    let center = Vector3D::new([0.0, 0.0, 0.0]);
    // FIXME: handle movement and rotation properly
    // FIXME: movement should be done in the view coordinate system
    let view_from = Transformation::new()
        .rotate(camera.rotation)
        .translate(camera.position)
        .apply(Vector3D::new([3.0, 0.0, 0.0]));

    let view_at = Transformation::new().translate(camera.position).apply(center);
    let view = Transformation::new()
        .perspective_look(view_from, view_at)
        .zoom(camera.zoom)
        .view(image);

    if config.draw_grid {
        draw_grid(image, view);
    }
    let mut rendered_triangles = 0;
    for object in objects {
        // TODO: use simplified object structure (vertex list and face list with vertex indices) to reduce
        // transformation costs, currently every vertex is transformed multiple times
        for face in &object.faces {
            rendered_triangles = rendered_triangles + draw_face(image, &face, view_from, view, config);
        }
    }
    if config.draw_axes {
        draw_axes(image, center, 1.0, 2, view);
    }
    if config.draw_camera {
        draw_line_3d(image, view_at, center, Color::new(0xFFFFFF), 2, view);
        draw_vertex(image, view_at, 0.01, 2, Color::new(0xFFFFFF), view);
    }
    if config.draw_cursor {
        draw_cursor(image, cursor);
    }
    // TODO: render grid
    return rendered_triangles;
}

fn draw_face(image: &mut Image, face: &Face, ligth: Vector3D, view: Transformation, config: Config) -> u32 {
    // TODO: here we should only work with pixels and integer coordinates
    let intensity = face.normal.dot(ligth.normalize());
    if intensity < 0.0 {
        return 0;
    }
    let mut vertices = face.vertices;
    for i in 0..vertices.len() {
        // FIXME: this should happen before calling this function
        let transformed_vector = view.apply(vertices[i].position);
        vertices[i].set_position(transformed_vector);
    }
    vertices.sort_by(|a, b| a.position.y().partial_cmp(&b.position.y()).unwrap());
    let a = vertices[0];
    let b = vertices[1];
    let c = vertices[2];

    // TODO: add of_face to BoundingBox
    let bounding_box = BoundingBox::of_vectors(&[a.position, b.position, c.position]);
    if !bounding_box.intersects(&BoundingBox::of_image(image)) {
        return 0;
    }

    // TODO: https://www.scratchapixel.com/lessons/3d-basic-rendering/rasterization-practical-implementation/rasterization-stage
    if config.draw_fill {
        vertices.sort_by(|a, b| a.position.y().partial_cmp(&b.position.y()).unwrap());
        let a = vertices[0];
        let b = vertices[1];
        let c = vertices[2];

        let delta_ab = vertex_delta(&a, &b);
        let mut lerp_ab = a.interpolate(b, delta_ab);

        let delta_bc = vertex_delta(&b, &c);
        let mut lerp_bc = b.interpolate(c, delta_bc);

        let delta_ac = vertex_delta(&a, &c);
        let mut lerp_ac = a.interpolate(c, delta_ac);

        let y_from = std::cmp::max(a.position.y() as i32, 0);
        let y_mid = b.position.y() as i32;
        let y_to = std::cmp::min(c.position.y() as i32, image.height as i32);

        for y in y_from..=y_to {
            let from: Vertex;
            if y < y_mid {
                advance_vertex(&mut lerp_ab, y);
                from = Vertex::from(&lerp_ab.value);
            } else {
                advance_vertex(&mut lerp_bc, y);
                from = Vertex::from(&lerp_bc.value);
            };
            advance_vertex(&mut lerp_ac, y);
            let to = Vertex::from(&lerp_ac.value);
            scanline_vertices(image, y as Size, from, to, face.texture, intensity, config);
        }
    }
    if face.outline.is_some() && config.draw_wireframe {
        let color = face.outline.unwrap();
        draw_line_2d(image, a.position, b.position, color, 1);
        draw_line_2d(image, b.position, c.position, color, 1);
        draw_line_2d(image, a.position, c.position, color, 1);
    }
    if config.draw_normals {
        draw_line_3d(
            image,
            face.center,
            face.center + face.normal * 0.1,
            Color::new(0xFFFFFF),
            1,
            view,
        );
    }
    if config.draw_bounding_box {
        draw_bounding_box(image, bounding_box);
    }
    return 1;
}

fn draw_line_3d(
    image: &mut Image,
    mut a: Vector3D,
    mut b: Vector3D,
    color: Color,
    thickness: u32,
    view: Transformation,
) {
    a = view.apply(a);
    b = view.apply(b);
    draw_line_2d(image, a, b, color, thickness);
}

fn draw_line_2d(image: &mut Image, mut a: Vector3D, mut b: Vector3D, color: Color, thickness: u32) {
    let bounding_box = BoundingBox::of_vectors(&[a, b]);
    if !bounding_box.intersects(&BoundingBox::of_image(image)) {
        return;
    }
    if a.y() > b.y() {
        let temp = a;
        a = b;
        b = temp;
    }
    let delta = delta(&a, &b);
    let mut lerp_ab = a.interpolate(b, delta);
    advance_vector(&mut lerp_ab, 0);
    let y_from = lerp_ab.value[1] as i32;
    let y_to = std::cmp::min(b.y() as i32, image.height as i32);
    let mut x_from = lerp_ab.value[0];

    for y in y_from..=y_to {
        advance_vector(&mut lerp_ab, y as i32);
        let x_to = lerp_ab.value[0];
        // FIXME: should pass z values in order to have proper depth buffer
        scanline(image, y as Size, x_from, x_to, color, thickness);
        x_from = x_to;
    }
}

fn vertex_delta(from: &Vertex, to: &Vertex) -> f32 {
    delta(&from.position, &to.position)
}

fn delta(from: &Vector3D, to: &Vector3D) -> f32 {
    (to.x() - from.x()).abs().max((to.y() - from.y()).abs())
}

fn advance_vertex(lerp: &mut VertexInterpolation, y: i32) {
    // FIXME: use a better approach than iteration
    while !lerp.finished && lerp.value[1] as i32 <= y {
        lerp.advance();
    }
}

fn advance_vector(lerp: &mut VectorInterpolation, y: i32) {
    // FIXME: use a better approach than iteration
    while !lerp.finished && lerp.value[1] as i32 <= y {
        lerp.advance();
    }
}

fn scanline_vertices(
    image: &mut Image,
    y: Size,
    mut from: Vertex,
    mut to: Vertex,
    texture: Option<&Image>,
    intensity: f32,
    config: Config,
) {
    if from.position.x() > to.position.x() {
        let temp = to;
        to = from;
        from = temp;
    }
    if from.position.x() >= image.width as f32 || to.position.x() < 0.0 {
        return;
    }
    // TODO: skip line rendering if Z values are smaller then the ones in the Z buffer
    let mut lerp_x = from.interpolate(to, to.position.x() - from.position.x());
    if from.position.x() < 0.0 {
        lerp_x.advance_by(from.position.x());
    }
    let x_from = clip_x(image, from.position.x());
    let x_to = clip_x(image, to.position.x());
    for x in x_from..x_to {
        let point = Vertex::from(&lerp_x.value);
        lerp_x.advance();
        let z = point.position.z();
        let color = if config.draw_z_buffer {
            Color::of_z_index(z)
        } else if texture.is_some() {
            // FIXME: fix texture rendering with projection
            // https://github.com/ssloy/tinyrenderer/wiki/Technical-difficulties-linear-interpolation-with-perspective-deformations
            texture
                .unwrap()
                .get_pixel(point.texture.x() as Size, point.texture.y() as Size)
                .with_intensity(intensity)
        } else {
            point.color.with_intensity(intensity)
        };
        image.set_pixel_with_depth(x, y, z, color);
    }
}

fn scanline(image: &mut Image, y: Size, mut x_from: f32, mut x_to: f32, color: Color, thickness: u32) {
    if x_from > x_to {
        let temp = x_to;
        x_to = x_from;
        x_from = temp;
    }
    if x_from >= image.width as f32 || x_to < 0.0 {
        return;
    }
    let from = clip_x(image, x_from - (thickness - 1) as f32 / 2.0);
    let to = clip_x(image, x_to + (thickness - 1) as f32 / 2.0);

    for x in from..=to {
        image.set_pixel(x, y as Size, color);
    }
}

fn clip_x(image: &Image, x: f32) -> Size {
    return clip(x, 0, image.width - 1);
}

fn clip_y(image: &Image, y: f32) -> Size {
    return clip(y, 0, image.height - 1);
}

fn clip(i: f32, min: Size, max: Size) -> Size {
    return if i < min as f32 {
        min
    } else if i > max as f32 {
        max
    } else {
        i as Size
    };
}

fn draw_cursor(image: &mut Image, cursor: Option<(f32, f32)>) {
    let crosshair = if cursor.is_some() {
        Vector3D::new([cursor.unwrap().0, cursor.unwrap().1, 0.0])
    } else {
        Vector3D::new([image.width as f32 / 2.0, image.height as f32 / 2.0, 0.0])
    };
    draw_crosshair(image, crosshair, Color::new(0xFFFFFF));
}

fn draw_crosshair(image: &mut Image, center: Vector3D, color: Color) {
    let horizontal_from = Vector3D::new([0.0, center.y(), 0.0]);
    let horizontal_to = Vector3D::new([image.width as f32, center.y(), 0.0]);
    draw_line_2d(image, horizontal_from, horizontal_to, color, 1);

    let vertical_from = Vector3D::new([center.x(), 0.0, 0.0]);
    let vertical_to = Vector3D::new([center.x(), image.height as f32, 0.0]);
    draw_line_2d(image, vertical_from, vertical_to, color, 1);
}

fn draw_axes(image: &mut Image, center: Vector3D, length: f32, thickness: u32, view: Transformation) {
    let x_axis = Vector3D::new([length, 0.0, 0.0]);
    let y_axis = Vector3D::new([0.0, length, 0.0]);
    let z_axis = Vector3D::new([0.0, 0.0, length]);
    draw_line_3d(image, x_axis, center, Color::new(0xFF0000), thickness, view);
    draw_line_3d(image, y_axis, center, Color::new(0x00FF00), thickness, view);
    draw_line_3d(image, z_axis, center, Color::new(0x0000FF), thickness, view);
}

fn draw_vertex(image: &mut Image, v: Vector3D, size: f32, thickness: u32, color: Color, view: Transformation) {
    for i in 0..3 {
        let mut from: Vector3D = v;
        let mut to: Vector3D = v;
        from.value[i] += size;
        to.value[i] -= size;
        draw_line_3d(image, from, to, color, thickness, view);
    }
}

fn draw_bounding_box(image: &mut Image, bounding_box: BoundingBox) {
    draw_rectangle(image, bounding_box.min, bounding_box.max, Color::new(0xFFFFFF), 1)
}

fn draw_rectangle(image: &mut Image, a: Vector3D, b: Vector3D, color: Color, thickness: u32) {
    draw_line_2d(
        image,
        Vector3D::new([a.x(), a.y(), 0.0]),
        Vector3D::new([b.x(), a.y(), 0.0]),
        color,
        thickness,
    );
    draw_line_2d(
        image,
        Vector3D::new([a.x(), b.y(), 0.0]),
        Vector3D::new([b.x(), b.y(), 0.0]),
        color,
        thickness,
    );
    draw_line_2d(
        image,
        Vector3D::new([a.x(), a.y(), 0.0]),
        Vector3D::new([a.x(), b.y(), 0.0]),
        color,
        thickness,
    );
    draw_line_2d(
        image,
        Vector3D::new([b.x(), a.y(), 0.0]),
        Vector3D::new([b.x(), b.y(), 0.0]),
        color,
        thickness,
    );
}

fn draw_grid(image: &mut Image, view: Transformation) {
    let step = 0.1;
    let min = -10;
    let max = 10;
    for x in min..=max {
        draw_line_3d(
            image,
            Vector3D::new([x as f32 * step, 0.0, min as f32 * step]),
            Vector3D::new([x as f32 * step, 0.0, max as f32 * step]),
            Color::new(0xFFFFFF),
            1,
            view,
        );
    }
    for z in min..=max {
        draw_line_3d(
            image,
            Vector3D::new([min as f32 * step, 0.0, z as f32 * step]),
            Vector3D::new([max as f32 * step, 0.0, z as f32 * step]),
            Color::new(0xFFFFFF),
            1,
            view,
        );
    }
}
