use crate::lib::color::Color;

// TODO: use a better name and struct e.g. Pixel
pub type Size = u32;

#[derive(Debug)]
pub struct Image {
    pub width: Size,
    pub height: Size,
    pub buffer: Vec<u32>,
    // TODO: use a properly scaled z buffer
    pub z_buffer: Vec<f32>,
    buffer_size: Size,
}

// TODO: use generics and different types for ZBuffer and PixelBuffer
impl Image {
    pub fn new(width: Size, height: Size) -> Image {
        let buffer_size = height * width;
        let buffer = vec![0; buffer_size as usize];
        let z_buffer = vec![f32::NEG_INFINITY; buffer_size as usize];
        Image {
            height,
            width,
            buffer_size,
            buffer,
            z_buffer,
        }
    }

    pub fn get_pixel(&self, x: Size, y: Size) -> Color {
        let offset = self.get_wrapped_offset(x, y);
        return Color::new(self.buffer[offset as usize]);
    }
    pub fn set_pixel_with_depth(&mut self, x: Size, y: Size, z: f32, color: Color) {
        let offset = self.get_offset(x, y);
        if offset < self.buffer_size && z > self.z_buffer[offset as usize] {
            self.buffer[offset as usize] = color.value;
            self.z_buffer[offset as usize] = z;
        }
    }
    pub fn set_pixel(&mut self, x: Size, y: Size, color: Color) {
        let offset = self.get_offset(x, y);
        if offset < self.buffer_size {
            self.buffer[offset as usize] = color.value;
        }
    }
    pub fn clear(&mut self) {
        // TODO: compare performance betwwen vector reset and low-level write
        unsafe {
            std::ptr::write_bytes(self.buffer.as_mut_ptr(), 0, self.buffer.len());
        }
        self.z_buffer = vec![f32::NEG_INFINITY; self.buffer_size as usize];
    }

    fn get_wrapped_offset(&self, x: Size, y: Size) -> Size {
        return ((y % self.height) * self.width) + (x % self.width);
    }

    fn get_offset(&self, x: Size, y: Size) -> Size {
        return (y * self.width) + x;
    }
}
